# Changelog
All notable changes to the Minecraft Server Systemd Services project will be
documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.0] - 2020-03-20
### Changed
- Set Systemd timer (minecraft-stats.timer) from monotonic time to realtime.
Default every 6 hours.
- Update documentation for updating timers.

## [1.2.1] - 2020-03-01
### Changed
- Remove redundant Systemd backup timer properties (minecraft-backup.timer).

## [1.2.0] - 2020-02-22
### Changed
- Set Systemd backup timer (minecraft-backup.timer) from monotonic time to
realtime and run every ten minutes.

## [1.0.1] - 2020-02-08
### Changed
- Correct step order, prompt and title (README.md).
- Correct spelling error (README.md).
- Correct clone URL (README.md).
- Add Systemd hyperlink (README.md).
- Add missing install steps for user and group (README.md).
- Add two assumptions (README.md).

## [1.0.0] - 2020-02-07
### Added
- CHANGELOG.md
- CONTRIBUTING.md
- LICENSE
- minecraft-paper.service
- minecraft-spigot.service
- minecraft-vanila.service
### Changed
- Correct step order, prompt and title (README.md).
- Systemd unit cleanup.
- Correct description of Systemd unit (minecraft-stats.service).
- Correct document formatting issues (README.md).
- Add Git dependency (README.md).
- Add hyper links (README.md).
- Add steps for editing Systemd units (README.md).
- Add example output (README.md).
- Add LICENSE reference (README.md).

### Removed
- Remove files not associated to the project.
- Remove redundant Systemd unit minecraft.service.
