# What Is Minecraft Server Systemd Services?

Systemd unit files for use with the Minecraft Server and associated services
(backup and stats).

# Features

- Supports use with Minecraft server software [PaperMC](https://papermc.io/),
[SpigotMC](https://www.spigotmc.org/) and
[Vanilla](https://www.minecraft.net/download/server/).
- Backup the Minecraft server directory.
  - To an archive.
  - Sync to a desired destination.
- Sync Minecraft stats data to [MinecraftStats](https://github.com/pdinklag/MinecraftStats).

# System Requirements

- GNU Linux operating System
- [Git](https://git-scm.com/)
- [Wardenctl](https://gitlab.com/adouglas/wardenctl) (optional if one replaces wardenctl commands)
- [Systemd](https://freedesktop.org/wiki/Software/systemd/)

# Installation

**Assumptions**

- Have an understanding of general server practices.
- Have experienced working knowledge within a CLI (command-line interface).
- Installed all required dependencies as stated in
[System Requirements](#system-requirements) section.
- Dependencies are accessible via
[environment PATH](https://wiki.archlinux.org/index.php/Environment_variables).
- Installation is done via Linux CLI.
- Steps prefixed with a "$" (dollar sign) represents the CLI prompt. The text
after the "$" is to be entered at the CLI.
- Steps prefixed with a "#" (number sign) represents the CLI prompt with
elevated user permissions (e.g. root). The text after the "#" is to be entered
at the CLI.
- A single backslash character beside another character is used to escape the
proceeding character. In this case backslash character is not to to be entered
at the CLI.
- These instructions are an example of installation and configuration.

## Manual

1. Change to your home directory.
   ```console
   $ cd ~
   ```
1. Download the project.
   ```console
   $ git clone https://gitlab.com/adouglas/minecraft-systemd-services.git
   ```
1. Change to the project directory.
   ```console
   $ cd minecraft-systemd-services
   ```
1. Copy Systemd unit files.
   ```console
   # cp minecraft-* /etc/systemd/system/
   ```
   ```
   minecraft-backup.service
   minecraft-backup.timer
   minecraft-paper.service
   minecraft-spigot.service
   minecraft-stats.service
   minecraft-stats.timer
   minecraft-vanilla.service
   ```
1. Edit each applicable unit file(s).
   - Ensure paths are correct (ReadWriteDirectories, [WorkingDirectory](https://www.freedesktop.org/software/systemd/man/systemd.exec.html),
[ExecStart](https://www.freedesktop.org/software/systemd/man/systemd.service.html),
[ExecStop](https://www.freedesktop.org/software/systemd/man/systemd.service.html)).
   - Ensure timers property "[OnCalendar](https://www.freedesktop.org/software/systemd/man/)" is set as
desired.
   ```console
   $ nano minecraft-backup.service
   ```
1. Set file permissions.
   ```console
   # chmod 644 /etc/systemd/system/minecraft-*
   ```
1. Set file ownership and group.
   ```console
   # chown root:root /etc/systemd/system/minecraft-*
   ```
1. Add user base upon "User" unit property value.
   ```
   # useradd -r -s /bin/bash minecraft
   ```
   ```
   -r create a system account.
   -s user's login shell.
   ```
1. Add group based upon "Group" unit property value.
   - **Note:** This step is only required if group name differs from username.
   ```
   # groupadd minecraft
   # usermod -a minecraft minecraft
   ```
   ```
   -a add the user to supplementary group.
   ```
1. Enable services.
   ```console
   # systemctl enable minecraft-paper.service
   # systemctl enable minecraft-backup.timer
   # systemctl enable minecraft-stats.timer
   ```

# Usage

For an even simpler usage at the CLI run "wardenctl help" without double quotes.

## Useful Command Examples

Action               | Command Example       | Notes
---------------------|-----------------------|--------
Reload all unit files| systemctl daemon-reload | Required after modifying unit file(s).
Restart unit         | systemctl restart minecraft-paper.service |
Start unit           | systemctl start minecraft-paper.service |
Stop unit            | systemctl stop minecraft-paper.service |
View unit status     | systemctl status minecraft-paper.service |
View unit messages   | journalctl -u minecraft-paper.service |
View unit messages   | journalctl -fu minecraft-paper.service | View most recent entries and follow new entries as they occur. Press CTRL-C to quit.

# License

Minecraft Server Systemd Services is licensed under the GNU General Public
License v3.0. Review the license by viewing the LICENSE file.

# Copyright Notice

Copyright (c) 2019-2020 Adam Douglas Some Rights Reserved
